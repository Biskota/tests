package api.registration;


import api.cookie.SessionCookie;
import api.pages.MainPage;
import api.registration.RegistrationResponse;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.junit.jupiter.api.*;


import static api.constants.ResultValues.ALREADY_REGISTERED_EMAIL_MESSAGE;
import static api.constants.ResultValues.INVALID_EMAIL_MESSAGE;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class RegistrationTests {
    RegistrationResponse response = new RegistrationResponse();
    private static Cookie mainPageCookie;

    @BeforeAll
    @Tag("API")
    @DisplayName("Open product page")
    static void setCookieAndToken() {
        mainPageCookie = SessionCookie.getCookie(MainPage.openMainPage());
    }

    @Test
    @Tag("API")
    @DisplayName("Verify email field with invalid data")
    public void invalidEmailTest() {
        Response response1 = response.registrationInvalidEmail();
        String actualError = response1.jsonPath().getString("errors[0]");
        Boolean actualHasError = response1.jsonPath().get("hasError");
        String expectedError = INVALID_EMAIL_MESSAGE;
        Assertions.assertAll(
                () -> assertEquals(200, response1.statusCode()),
                () -> assertTrue(actualHasError),
                () -> assertEquals(expectedError, actualError)
        );
    }

    @Test
    @Tag("API")
    @DisplayName("Verify email field with already registered email")
    public void alreadyRegisteredEmailTest() {
        Response response1 = response.registrationAlredyRegistred();
        String actualError = response1.jsonPath().getString("errors[0]");
        Boolean actualHasError = response1.jsonPath().get("hasError");
        String expectedError = ALREADY_REGISTERED_EMAIL_MESSAGE;
        Assertions.assertAll(
                () -> assertEquals(200, response1.statusCode()),
                () -> assertTrue(actualHasError),
                () -> assertEquals(expectedError, actualError)
        );
    }

}