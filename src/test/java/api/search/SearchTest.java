package api.search;

import api.constants.ResultValues;
import io.restassured.response.Response;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;


import static org.junit.jupiter.api.Assertions.assertEquals;

public class SearchTest {
    SearchField searchField = new SearchField();

    @Tag("API")
    @DisplayName("Verify search field with data from footer")
    @ParameterizedTest
    @MethodSource("api.util.DataUtils#provideArgumentsForFooterSearch")
    void searchByDataFromFooterWithoutLogIn(String search) {
        Response response = searchField.search(search);
        String expectedError = ResultValues.SEARCH_NOT_FOUND_MESSAGE + "\"" + search + "\"";
        String result = response.htmlPath().
                getString("**.find { it.@class == 'alert alert-warning' }");
        Assertions.assertAll(
                () -> assertEquals(200, response.statusCode()),
                () -> assertEquals(expectedError, result.trim())
        );
    }
}