package ui.constants;

public class URL {
    private URL() {
    }

    public static final String BASE_URL = "http://automationpractice.com/index.php";

    public static final String AUTHENTICATION_URL = BASE_URL + "?controller=authentication";
    public static final String MY_ACCOUNT_URL = BASE_URL + "?controller=my-account";
}
