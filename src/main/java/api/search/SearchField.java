package api.search;

import api.connection.RestAPIConnection;
import api.constants.URL;
import io.restassured.response.Response;

public class SearchField {

    public Response search(String searchContent) {
        return RestAPIConnection.connectionHTML()
                .when()
                .params("controller", "search",
                        "orderby", "position",
                        "orderway", "desc",
                        "search_query", searchContent,
                        "submit_search", "")
                .get(URL.DEFAULT_URL);
    }

}
